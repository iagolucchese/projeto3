﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallJumpTriggerBehavior : MonoBehaviour
{
    public delegate void OnWallJumpTrigger(bool contactDetected, float flip);
    public static OnWallJumpTrigger wallJumpTrigger;

    public float flip;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D other) {
        if (wallJumpTrigger != null)
            wallJumpTrigger(true, flip);
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (wallJumpTrigger != null)
            wallJumpTrigger(false, flip);
    }
}
