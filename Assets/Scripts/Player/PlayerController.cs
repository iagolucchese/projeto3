﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.Events;
using Prime31; //codigo de player importado

[System.Serializable]
public class PlayerController : MonoBehaviour
{
	private static PlayerController _instance;
	public static PlayerController Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("No PlayerController instance.");
            }
            return _instance;
        }
    }
	
	public Transform AnimationsTransform;
    public CheckpointBehavior LatestCheckpoint;
    public SpriteRenderer ScreenTransitionPanel;

    private GameObject Cachecol;
    private PlayerParticleSystemManager ppsm;

	public UnityAction OnPlayerDied;

	#region MovSettings
	[Header("Movement Settings")]
	public Vector3 _velocity;
	public float gravity = -50f;
	public float runSpeed = 15f;
	public float groundDamping = 20f;
	public float inAirDamping = 10f;
	public float wallSlideSlowdownFactor = 0.25f;
	public float terminalSpeed = 40f;
	public float OnWallSlideVelocityModifier = 0.1f;
    public bool isWalkingBlocked { get; set; }
	public bool isMovementLocked;
    public bool isDead { get; set; }
	#endregion

	#region Jump
	[Header("Jump Settings")]
	public float jumpHeight = 5f;
    public float maxTimeHoldingJumpButton = 0.15f;
	public float timeJumpButtonHeldDown = 0f;
	public int framesAfterUngroundedAllowJump = 10;
	private int framesAfterGround = 0;
	public float jumpCooldown = 0.1f;
	private float jumpCooldownCounter = 0f;
	#endregion

	#region WallJump
    [Header("Wall Jump Settings")]
	public bool canWallJump = false;
	public bool wallJumpUnlocked = true;
	public Vector2 wallJumpForce = new Vector2 (7f, 5f);
	public float airDampingSeconds = 0.2f;
	public bool flipWalljumpDirection = false;
	private float storedWalljumpDirection = 0f;
	#endregion

	#region Dash
	[Header("Dash Settings")]
	public bool canDash = false;
	public bool dashUnlocked = true;
	public bool resetDashWhenGrounded = true;
	public Vector2 dashForce = new Vector2(5f, 5f);
	public float dashDuration = 0.3f;
	public float dashCooldown = 0.6f;
	private float dashCooldownCounter = 0f;
	public bool isDashing = false;
	#endregion

	#region Misc
	[Header("Misc")]
	public RoomManager CurrentRoom;
	public CharacterController2D _controller;

	private bool ignoreGroundDamping = false;
    private bool ignoreAirDamping = false;
	private bool ignoreGravity = false;
	private float normalizedHorizontalSpeed = 0;
    private float StoredGroundDamping;

	private InputManager _input;
	private Animator _animator;
    private TrailRenderer _trailRenderer;
	private RaycastHit2D _lastControllerColliderHit;    
	private Vector3 storedVelocity;
	private AudioController AudioController;
	#endregion

	void Awake()
	{
		//singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

		//setando variaveis
		_input = InputManager.Instance;
		_animator = GetComponentInChildren<Animator>();
		_controller = GetComponent<CharacterController2D>();
        Cachecol = GameObject.FindGameObjectWithTag("Cachecol");
        ppsm = GetComponentInChildren<PlayerParticleSystemManager>();
		AudioController = AudioController.Instance;

        StoredGroundDamping = groundDamping;

        if (!AnimationsTransform)
		{
            AnimationsTransform = _animator.gameObject.transform;
        }
		WallJumpTriggerBehavior.wallJumpTrigger += WallJumpContact;
	}

	private void WallJumpContact(bool hasContact, float flip)
	{
		if (hasContact && !_controller.isGrounded)
		{
			if (!canWallJump)
				storedWalljumpDirection = transform.localScale.x * flip;
			canWallJump = true;
		}
		else
			canWallJump = false;
	}

	#region Movement Functions

	private void FlipPlayer()
	{
		transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );
		AnimationsTransform.localScale = new Vector3(-AnimationsTransform.localScale.x, AnimationsTransform.localScale.y, AnimationsTransform.localScale.z);
		
        FlipPlayerSprite();
    }

    private void ResetPlayerRotation()
    {
        transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        AnimationsTransform.localScale = new Vector3(Mathf.Abs(AnimationsTransform.localScale.x), AnimationsTransform.localScale.y, AnimationsTransform.localScale.z);
        AnimationsTransform.localRotation = Quaternion.identity;
    }

    private void FlipPlayerSprite()
    {
        AnimationsTransform.Rotate(0f, 180f, 0f);
    }
	
	private void movePlayerInDirection(float direction)
	{
		normalizedHorizontalSpeed = direction;
		_animator.SetBool("Walking", true);

		 //flipa o player inteiro, se necessario\
		if ((transform.localScale.x < 0f && direction > 0) || (transform.localScale.x > 0f && direction < 0) && !isWalkingBlocked)
		{
			FlipPlayer();
		}
	}

	private void horizontalMovement() //walking
	{
		if(!isWalkingBlocked && _input.HorizontalInput != 0)
		{
			movePlayerInDirection(_input.HorizontalInput);
		}
		else
		{
			normalizedHorizontalSpeed = 0;
			_animator.SetBool("Walking", false);
		}
	}
	
	private void verticalMovement() //jump and walljump
	{
		if (isWalkingBlocked) return;
		if(!isDashing && _input.JumpButtonDown)
		{
			if(framesAfterGround > 0 && jumpCooldownCounter <= 0f) //if (_controller.isGrounded && jumpCooldownCounter <= 0f)
			{
				_velocity.y = Mathf.Sqrt(jumpHeight * -gravity);
				_animator.SetBool("Jumping", true);
                _animator.SetBool("Falling", false);
                jumpCooldownCounter = jumpCooldown;

				//audio
				AudioController.PlaySound("Jump");
			}
			else if (wallJumpUnlocked && canWallJump && jumpCooldownCounter <= 0f)
			{
				jumpCooldownCounter = jumpCooldown;
				canWallJump = false;
				_velocity.x = -storedWalljumpDirection * (Mathf.Sqrt(wallJumpForce.x * -gravity) * (flipWalljumpDirection ? -1f : 1f));
				_velocity.y = Mathf.Sqrt(wallJumpForce.y * -gravity);
				ignoreAirDamping = true;
				Invoke("AirDampingToNormal", airDampingSeconds);
				_animator.SetBool("WallJumping", true);
                _animator.SetBool("Falling", false);

                //flipa o player no walljump
                FlipPlayer();

				//audio
				AudioController.PlaySound("Jump");

				timeJumpButtonHeldDown = maxTimeHoldingJumpButton; //doesnt let the player hold the button to jump higher on walljumps
			}
		}
		else if (_input.JumpButton && !_controller.isGrounded)
		{
			if (timeJumpButtonHeldDown < maxTimeHoldingJumpButton && !isDashing) //fix do superdash
			{
				_velocity.y += Mathf.Sqrt(-gravity * jumpHeight * Time.fixedDeltaTime);
				timeJumpButtonHeldDown += Time.fixedDeltaTime;
				/* if (SuperDashTrail && dashCooldownCounter > 0 && !SuperDashTrail.isPlaying)
					SuperDashTrail.Play(); */
			}
		} 
		else
		{
			timeJumpButtonHeldDown = maxTimeHoldingJumpButton;
		}
	}

	private void AirDampingToNormal()
	{
		ignoreAirDamping = false;
	}

	private bool solvePlayerDashing()
	{
		//if this returns true, it will let movement be solved on this frame as well, otherwise it only solves dashing
		if (!isWalkingBlocked && dashUnlocked && canDash && _input.DashButtonDown)
		{
			canDash = false;
			isDashing = true;
			dashCooldownCounter = dashCooldown;
			timeJumpButtonHeldDown = maxTimeHoldingJumpButton*2;

			Vector2 direction = new Vector2(_input.HorizontalInput, _input.VerticalInput);
			if (_input.VerticalInput == 0 && _input.HorizontalInput == 0)
			{
				direction.x = transform.localScale.x > 0 ? 1f : -1f;
				direction.y = 0;
			}
			direction.Normalize();
			_velocity = direction * dashForce * Time.fixedDeltaTime * -gravity;

			//TODO: iterar essas mecanicas de dash
			isWalkingBlocked = true;
            ignoreGroundDamping = true;
            ignoreAirDamping = true;
			ignoreGravity = true;

			if (ppsm.DashTrail)
			{
                ppsm.DashTrail.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, Mathf.Rad2Deg * Mathf.Atan2(direction.y, direction.x)));
                ppsm.DashTrail.Play();
			}			
            Invoke("EndDash", dashDuration);
            _animator.SetBool("Dashing", true);

			//audio
			AudioController.PlaySound("Dash");

            return false;
		}
		return true;
	}

	private void EndDash()
    {
		if (ppsm.DashTrail)
            ppsm.DashTrail.Stop(true, ParticleSystemStopBehavior.StopEmitting);
		if (ppsm.SuperDashTrail)
            ppsm.SuperDashTrail.Stop(true, ParticleSystemStopBehavior.StopEmitting);
		
		isDashing = false;
        isWalkingBlocked = false;
        ignoreGroundDamping = false;
        ignoreAirDamping = false;
		ignoreGravity = false;
        _animator.SetBool("Dashing", false);
    }

	private void solvePlayerMovement()
	{
        if ( _controller.isGrounded )
		{
			if (!_animator.GetBool("Grounded"))
				AudioController.PlaySound("Landed");
			_velocity.y = 0;
			framesAfterGround = framesAfterUngroundedAllowJump;
		}
		else
		{
			if (framesAfterGround > 0)
				framesAfterGround--;
		}
		_animator.SetBool("Grounded", _controller.isGrounded);

		if (solvePlayerDashing()) 
		{
			if (dashCooldownCounter > 0f)
				dashCooldownCounter -= Time.fixedDeltaTime;
			if (jumpCooldownCounter > 0f)
				jumpCooldownCounter -= Time.fixedDeltaTime;
			verticalMovement();
			horizontalMovement();
		}

		var smoothedMovementFactor = _controller.isGrounded ? (ignoreGroundDamping ? 1f : groundDamping) : (ignoreAirDamping ? 1f : inAirDamping); // how fast do we change direction?
		var actualMoveSpeed = normalizedHorizontalSpeed  * runSpeed;
		_velocity.x = Mathf.Lerp(_velocity.x, actualMoveSpeed, Time.fixedDeltaTime * smoothedMovementFactor );

        float appliedGravity = 0f;
        if (_velocity.y < 0 && !_controller.isGrounded && //if falling
            (_input.HorizontalInput > 0 && _controller.collisionState.right //and if sliding down walls
            || _input.HorizontalInput < 0 && _controller.collisionState.left)) //while holding down the respective directional button
        {
            appliedGravity = gravity * wallSlideSlowdownFactor * Time.fixedDeltaTime; //cut the effect of gravity

            //gambiarra
            if (!_animator.GetBool("WallSliding"))
            {
                FlipPlayerSprite();
				_velocity.y = _velocity.y * OnWallSlideVelocityModifier; //gambi³
            }
            _animator.SetBool("WallSliding", true);
			if (!ppsm.WallSlideTrail.isPlaying)
				ppsm.WallSlideTrail.Play();
        }
        else
        {
            appliedGravity = gravity * Time.fixedDeltaTime;

            //gambiarra
            if (_animator.GetBool("WallSliding"))
            {
                FlipPlayerSprite();
            }

            _animator.SetBool("WallSliding", false);
			if (ppsm.WallSlideTrail.isPlaying)
				ppsm.WallSlideTrail.Stop(false, ParticleSystemStopBehavior.StopEmitting);
        }

		_velocity.y += appliedGravity * (ignoreGravity ? 0f : 1f);
		
		if (_controller.isGrounded && _velocity.y < 0)
		{
			timeJumpButtonHeldDown = 0; //resets the players jump when ground is touched
			if (resetDashWhenGrounded && dashCooldownCounter <= 0f) canDash = true;
			_animator.SetBool("Falling",false);
			//_animator.SetBool("Jumping",false);
			if(_input.VerticalInput < 0 && _velocity.y < 0)
			{
				_velocity.y *= 3f;
				_controller.ignoreOneWayPlatformsThisFrame = true;
			}
		}
		else
		{
			if (!_animator.GetBool("Falling") && _velocity.y < 0)
			{
				timeJumpButtonHeldDown = maxTimeHoldingJumpButton; //to avoid letting the player jump after he starts falling
				_animator.SetBool("Falling",true);
				_animator.SetBool("Jumping", false);
                _animator.SetBool("WallJumping", false);
            }			
		}

        //gambiarra²
        //if (_animator.GetBool("WallJumping"))
        //    _animator.SetBool("WallJumping", false);

        //terminal speed check
        if (_velocity.y <= -terminalSpeed) 
			_velocity.y = -terminalSpeed;
		
		_controller.move( _velocity * Time.fixedDeltaTime );
		// grab our current _velocity to use as a base for all calculations
		_velocity = _controller.velocity;
	}

	public void BlockPlayerMovement(bool resetSpeed = true, bool ignoreGravity = false, bool blockAllMovement = false)
	{
		isWalkingBlocked = true;
		if (!resetSpeed)
		{
			storedVelocity = _velocity;
		}
		if (ignoreGravity) ignoreGravity = true;
		if (blockAllMovement) 
		{
			isMovementLocked = true;
			_animator.speed = 0;
		}
		normalizedHorizontalSpeed = 0;
		_velocity.x = 0;
		timeJumpButtonHeldDown = maxTimeHoldingJumpButton;
	}

	public void BlockPlayerMovement()
	{
		bool resetSpeed = true;
		ignoreGravity = false;

		isWalkingBlocked = true;

		if (!resetSpeed)
		{
			storedVelocity = _velocity;
		}
		if (ignoreGravity) ignoreGravity = true;
		normalizedHorizontalSpeed = 0;
		_velocity.x = 0;
		timeJumpButtonHeldDown = maxTimeHoldingJumpButton;
	}

	public void UnblockPlayerMovement(bool resetSpeed, bool refreshDash, bool wasIgnoringGravity)
	{
		isWalkingBlocked = false;
		isMovementLocked = false;

		_animator.speed = 1;

		if (!resetSpeed)
		{
			_velocity = storedVelocity;
			storedVelocity = Vector3.zero;
		}
		if (refreshDash && dashUnlocked) 
			canDash = true;
		if (wasIgnoringGravity)
			ignoreGravity = false;
	}

	public void UnblockPlayerMovement()
	{
		var resetSpeed = true;
		var refreshDash = false;
		var wasIgnoringGravity = false;

		isWalkingBlocked = false;
		if (!resetSpeed)
		{
			_velocity = storedVelocity;
			storedVelocity = Vector3.zero;
		}
		if (refreshDash && dashUnlocked) 
			canDash = true;
		if (wasIgnoringGravity)
			ignoreGravity = false;
	}

	#endregion

	public void HandlePlayerDeath(float respawnInSeconds = 0.5f)
	{
		_velocity.x = 0;
		_velocity.y = 0;
		isDead = true;
		canDash = false;
		canWallJump = false;        

        //TODO resetar mais coisas?

        _animator.SetBool("Dead", true);
        _animator.SetBool("Jumping", false);
		_animator.SetBool("WallJumping", false);
		_animator.SetBool("WallSliding", false);

		ppsm.WallSlideTrail.Stop();
		ppsm.DashTrail.Stop();

        BlockPlayerMovement();

		Cachecol.SetActive(false);

		SceneController.DeathCounter++;
	}



    public void PlayDeathPS()
    {
        AnimationsTransform.gameObject.SetActive(false);
        ppsm.PlayerDeath.Play();
		AudioController.PlaySound("GlassShattering");
        SceneController.DoDeathScreenTransition(0.5f);
    }

    public void RespawnAtCheckpoint()
	{
		if (OnPlayerDied != null)
			OnPlayerDied.Invoke();
		isDead = false;
		_animator.SetBool("Dead", false);
		UnblockPlayerMovement(true, false, false);
		if (CurrentRoom != LatestCheckpoint.room)
		{
			SceneController.Instance.TransitionToVCam(LatestCheckpoint.room.RoomCamera, LatestCheckpoint.room.IsDarkRoom);
		}
		transform.position = LatestCheckpoint.transform.position;

        //corrigindo o bug que a kassiana achou
        ResetPlayerRotation();

        if (FollowerBehavior.Instance && FollowerBehavior.Instance.doFollow)
			FollowerBehavior.Instance.TeleportToTarget = true;
		AnimationsTransform.gameObject.SetActive(true);
		Cachecol.SetActive(true);
	}

    public void UnlockWalljump()
    {
        wallJumpUnlocked = true;
    }

    public void UnlockDash()
    {
        dashUnlocked = true;
    }

    public void ResetDash()
    {
        if (dashUnlocked)
        {
            canDash = true;
        }
    }

    public void SetGroundDamping(float newDamping)
    {
        groundDamping = newDamping;
    }

    public void ResetGroundDamping()
    {
        groundDamping = StoredGroundDamping;
    }

    public void StartedPushing()
    {
        _animator.SetBool("Pushing", true);
        //TODO som?
    }

    public void StoppedPushing()
    {
        _animator.SetBool("Pushing", false);
    }

	void Update()
	{
		if (Time.timeScale <= 0) //se jogo pausado, nao faz mais update
			return;		
		if (isDead)
			return;		
 	}

	private void FixedUpdate() 
	{
		if (Time.timeScale <= 0) //se jogo pausado, nao faz mais update
			return;
		if (isDead)
		{
			// pro caso do player morrer no ar
			/* _velocity.y += gravity * Time.fixedDeltaTime; 
			_controller.move( _velocity * Time.fixedDeltaTime );
			_velocity = _controller.velocity; */
			return;
		}
		if (!isMovementLocked)
			solvePlayerMovement();		
	}

	private void OnDestroy() {
		WallJumpTriggerBehavior.wallJumpTrigger -= WallJumpContact;
	}
}
