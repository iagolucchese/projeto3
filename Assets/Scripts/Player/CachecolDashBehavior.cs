﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CachecolDashBehavior : MonoBehaviour
{
    public AnimationCurve DashCurve;
    public float AnimationSpeed;
    private Material CachecolMaterial;
    private PlayerController pc;    
    private float lerp;

    void Start()
    {
        pc = PlayerController.Instance;
        CachecolMaterial = GetComponent<Renderer>().material;
    }

    void Update()
    {
        if (!pc.dashUnlocked)
        {
            lerp = 1f;
        }
        else if (pc.canDash)
        {
            lerp -= Time.deltaTime * AnimationSpeed;
        }
        else
        {
            lerp += Time.deltaTime * AnimationSpeed;
        }
        lerp = Mathf.Clamp01(lerp);

        if(CachecolMaterial.HasProperty("_LerpDissolve"))
        {
            CachecolMaterial.SetFloat("_LerpDissolve", DashCurve.Evaluate(lerp)*2f-1f);
        }
    }
}
