﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParticleSystemManager : MonoBehaviour
{
    public ParticleSystem DashTrail;
    public ParticleSystem PlayerDeath;
    public ParticleSystem WallSlideTrail;
    public ParticleSystem SuperDashTrail;
    
    void Start()
    {
        
    }

    void Update()
    {
        
    }
}
