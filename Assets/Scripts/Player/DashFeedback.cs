﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashFeedback : MonoBehaviour
{
    private PlayerController pc;
    private SpriteRenderer thisSprite;
    
    public Color canDashColor;
    public Color noDashColor;
    
    void Start()
    {
        pc = PlayerController.Instance;
        thisSprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (pc.canDash && pc.dashUnlocked)
            thisSprite.color = canDashColor;
        else
        {
            thisSprite.color = noDashColor;
        }
    }
}
