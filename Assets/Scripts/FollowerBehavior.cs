﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowerBehavior : MonoBehaviour
{    
    private static FollowerBehavior _instance;
	public static FollowerBehavior Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogWarning("No FollowerBehavior instance.");
            }
            return _instance;
        }
    }

    public GameObject followTarget;
    public bool doFollow = true;
    public bool Activated = false;
    public float smoothTime = 0.3f;
    public bool TeleportToTarget = false;

    private Vector2 direction;
    private Vector3 dif;

    private void Start() {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Update()
    {
        if (Activated && doFollow && followTarget)
        {
            transform.position = Vector2.SmoothDamp(transform.position, followTarget.transform.position, ref direction, (TeleportToTarget ? 0f : smoothTime));
            dif = transform.position - followTarget.transform.position;
            if (dif.x < 0)
                transform.localScale = new Vector3( -1, transform.localScale.y, transform.localScale.z );
            else
                transform.localScale = new Vector3( 1, transform.localScale.y, transform.localScale.z );
        }
        TeleportToTarget = false;
    }

    private void OnTriggerEnter2D(Collider2D other)        
    {
        if (!Activated && other.gameObject.tag == "Player")
        {
            doFollow = true;
            Activated = true;

            //cutscene
            CutsceneManager.Instance.PlayCutscene("CuscoGet");
        }
    }
}
