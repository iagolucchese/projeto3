﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

[System.Serializable]
public class ScreenTransitionBehavior : MonoBehaviour
{
    public Transform PlayerTargetPosition;
    public float TransitionDuration = 1f;
    public float lerpDerp = 0f;
    public float FadeInSpeed = 5f;
    public float FadeOutSpeed = 500f;
    public static bool DoingTransition;
    public bool MyTransition;
    //public Transform Checkpoint;

    private PlayerController player;
    private CinemachineConfiner cameraConfiner;

    private Vector3 storedPosition;
    private RoomManager parentRM;
    private RoomManager previousRoomRM;
    private SpriteRenderer transitionPanel;

    void Awake()
    {
        DoingTransition = false;
        parentRM = transform.parent.GetComponent<RoomManager>();
        if (!parentRM)
            Debug.LogError(gameObject.name + " transition has no parent room!");
        if (!player) player = PlayerController.Instance;
        transitionPanel = player.ScreenTransitionPanel;
    }

    private void Update() 
    {
        if (MyTransition && DoingTransition)
        {
            lerpDerp += Time.deltaTime * (1/TransitionDuration);
            lerpDerp = Mathf.Clamp01(lerpDerp);

            //transitionPanel.color = Color.black;
            transitionPanel.color = Color.Lerp(Color.clear, Color.black, lerpDerp * FadeOutSpeed);

            player.transform.position = Vector3.Lerp(storedPosition, PlayerTargetPosition.position, lerpDerp);
            
        }
        else if (lerpDerp > 0 && !DoingTransition)
        {
            lerpDerp -= Time.deltaTime * (1/TransitionDuration);
            lerpDerp = Mathf.Clamp01(lerpDerp);

            transitionPanel.color = Color.Lerp(Color.clear, Color.black, lerpDerp * FadeInSpeed);
            if (lerpDerp <= 0 || DoingTransition)
                MyTransition = false;
        }
    }

    private void TransitionScreen()
    {
        player.BlockPlayerMovement(false);
        player.gameObject.GetComponent<Collider2D>().enabled = false;
        
        storedPosition = player.transform.position;
        previousRoomRM = player.CurrentRoom;
        player.CurrentRoom = parentRM;
        parentRM.ToggleBackgrounds(false);
        //seta o ground damping para essa sala
        if (parentRM.SetPlayerGroundDamping > 0)
        {
            player.SetGroundDamping(parentRM.SetPlayerGroundDamping);   
        }
        else
        {
            player.ResetGroundDamping();
        }

        SceneController.Instance.TransitionToVCam(parentRM.RoomCamera, parentRM.IsDarkRoom);

        DoingTransition = true;
        MyTransition = true;
        Invoke("EndTransition", TransitionDuration);
    }

    private void EndTransition()
    {
        previousRoomRM.ToggleBackgrounds(true);

        player.UnblockPlayerMovement(false, true, false);
        player.gameObject.GetComponent<Collider2D>().enabled = true;

        DoingTransition = false;
        //MyTransition = false;
        lerpDerp = 1;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player" && !DoingTransition)
        {
            TransitionScreen();
        }
    }
}
