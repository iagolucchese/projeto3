﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointBehavior : MonoBehaviour
{
    public Material MirrorMaterial;
    public Material EmptyMirrorMaterial;
    private PlayerController player;
    public RoomManager room;
    private Camera mirrorCam;
    private MeshRenderer mirrorMesh;
    private ParticleSystem ps;

    void Start()
    {
        player = PlayerController.Instance;
        room = transform.parent.GetComponent<RoomManager>();
        mirrorCam = GetComponentInChildren<Camera>();
        mirrorMesh = GetComponentInChildren<MeshRenderer>();
        ps = GetComponentInChildren<ParticleSystem>();

        mirrorCam.gameObject.SetActive(false);
        if (!room)
        {
            //Debug.LogWarning("checkpoint " + name + " is not in a room(?)");
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
        {
            mirrorCam.gameObject.SetActive(true);
            //mirrorMesh.material = MirrorMaterial;
            var newMaterials = mirrorMesh.materials;
            for (int i = 0; i < mirrorMesh.materials.Length; i++)
            {
                newMaterials[i] = MirrorMaterial;
            }
            mirrorMesh.materials = newMaterials;

            if (player.LatestCheckpoint != this)
                if (ps) 
                    ps.Play();
            player.LatestCheckpoint = this;
            if (room && player.CurrentRoom != room)
            {
                player.CurrentRoom = room;
                room.ToggleBackgrounds(false);
            }
            if (!ScreenTransitionBehavior.DoingTransition && SceneController.Instance.ActiveCamera != player.CurrentRoom.RoomCamera)
            {
                //Debug.Log("Checkpoint changing the camera");
                SceneController.Instance.TransitionToVCam(player.CurrentRoom.RoomCamera, player.CurrentRoom.IsDarkRoom);
            }            
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
        {
            mirrorCam.gameObject.SetActive(false);
            var newMaterials = mirrorMesh.materials;
            for (int i = 0; i < mirrorMesh.materials.Length; i++)
            {
                newMaterials[i] = EmptyMirrorMaterial;
            }
            mirrorMesh.materials = newMaterials;
        }
    }
}
