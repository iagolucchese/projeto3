﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FadeInOutPanel : MonoBehaviour
{
    public string SceneName = "MainMenu";
    
    private void GoToNextScene()
    {
        SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
    }
}
