﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorByHeight : MonoBehaviour
{
    public Color colorBottom;
    public Color colorTop;

    public float bottom = 40f;
    public float top = 287f;

    private Camera cam;
    private float colorLerp;

    void Start()
    {
        cam = GetComponent<Camera>();        
    }

    void Update()
    {
        colorLerp = (PlayerController.Instance.transform.position.y + bottom)/top;
        cam.backgroundColor = Color.Lerp(colorBottom, colorTop, colorLerp);
    }
}
