﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPositionOnPlayerDeath : MonoBehaviour
{
    Vector3 StartingPosition;

    void Start()
    {
        StartingPosition = transform.position;
        PlayerController.Instance.OnPlayerDied += ResetPosition;
    }

    public void ResetPosition()
    {
        transform.position = StartingPosition;
    }
}
