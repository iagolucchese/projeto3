﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowerObjectOverTime : MonoBehaviour
{
    private Vector3 StartingPosition;
    private float lerp;
    private SpriteRenderer sprite;

    public bool Lowered = true;
    public Vector3 Destination;
    public float Speed = 1f;
    public bool DoLowerObject = false;
    public bool HideAfterMoving = true;
    

    void Start()
    {
        StartingPosition = transform.localPosition;
        sprite = GetComponent<SpriteRenderer>();
        sprite.enabled = true;
        this.gameObject.SetActive(true);
    }

    void Update()
    {
        if (!Lowered)
        {
            if (DoLowerObject)
            {
                lerp += Time.deltaTime * Speed;
            }
            else
            {
                lerp -= Time.deltaTime * Speed;
            }
            lerp = Mathf.Clamp01(lerp);
            transform.localPosition = Vector3.Lerp(StartingPosition, Destination, lerp);
            if (lerp >= 1 || lerp <= 0) 
            {   
                Lowered = true;
                gameObject.SetActive(!HideAfterMoving);
            }
        } 
    }

    public void LowerObject()
    {
        Lowered = false;
        DoLowerObject = true;
        gameObject.SetActive(true);
    }

    public void RaiseObjectBack()
    {
        Lowered = false;
        DoLowerObject = false;
        gameObject.SetActive(true);
    }
}
