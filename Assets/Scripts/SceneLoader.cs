﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public bool LoadOnStart = true;
    public string SceneName = "";

    void Start()
    {
        if (LoadOnStart && !string.IsNullOrEmpty(SceneName))
        {
            AsyncLoadScene();
        }
    }

    public void AsyncLoadScene()
    {
        StartCoroutine(LoadNewScene());
    }


    IEnumerator LoadNewScene()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(SceneName);
        while (!async.isDone) {
            yield return null;
        }
    }
}
