﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInputDebug : MonoBehaviour
{
    public Image jumpHUD;

    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetButton("Jump"))
            jumpHUD.color = Color.white;
        else
            jumpHUD.color = Color.clear;
    }
}
