﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EndingTextBehavior : MonoBehaviour
{
    public TextMeshProUGUI PlayTimeText;
    public TextMeshProUGUI DeathCounterText;

    void Start()
    {
        var timer = SceneController.PlayTime;
        int minutes = Mathf.FloorToInt(timer / 60F);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);
        string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);

        PlayTimeText.text = niceTime;
        DeathCounterText.text = SceneController.DeathCounter.ToString();
    }
}
