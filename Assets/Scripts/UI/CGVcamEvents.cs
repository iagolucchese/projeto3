﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGVcamEvents : MonoBehaviour
{
    private CutsceneManager cm;
    private Animator anim;
    private bool MaySkipCutscene;

    private void Start() 
    {
        cm = CutsceneManager.Instance;
        anim = GetComponent<Animator>();
    }

    private void Update() {        
        if (MaySkipCutscene && Input.anyKeyDown)
        {
            MaySkipCutscene = false;
            anim.SetBool("DoEndCutscene", true);
        }
        else if (anim.GetBool("DoEndCutscene"))
        {
            anim.SetBool("DoEndCutscene", false);
        }
    }

    public void LetSkipCutscene()
    {
        MaySkipCutscene = true;
    }

    public void CutsceneEnded()
    {
        cm.CutsceneEnded();
    }
}
