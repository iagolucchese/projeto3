﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTransitionPanelBehavior : MonoBehaviour
{
    private static DeathTransitionPanelBehavior _instance;
	public static DeathTransitionPanelBehavior Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("No PlayerController instance.");
            }
            return _instance;
        }
    }
    private PlayerController player;
    private Animator animator;

    void Start()
    {
        //singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        player = PlayerController.Instance;
        animator = GetComponent<Animator>();
    }

    public void DoDeathScreenTransition(int transition = 1)
    {
        animator.SetInteger("DoTransition", transition);
    }

    void TellPlayerToRespawn()
    {
        player.RespawnAtCheckpoint();
        animator.SetInteger("DoTransition", 0);
    }

    void GoToLastRoom()
    {
        SceneController.Instance.GoToLastRoom();
        animator.SetInteger("DoTransition", 0);
    }
}
