﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AntiMouseUnselect : MonoBehaviour
{
    public GameObject currentButton;
    EventSystem eventSystem;

    void Start ()
    {
        eventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        GameObject buttonObj = GameObject.Find("PlayText");
        buttonObj.GetComponent<Button>().onClick.AddListener(() => { currentButton = buttonObj; });
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            eventSystem.SetSelectedGameObject(currentButton);
    }
}
