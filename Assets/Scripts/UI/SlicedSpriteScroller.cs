﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlicedSpriteScroller : MonoBehaviour
{
    public float Speed = 5f;
    public SpriteRenderer frontR;

     private void Start() 
    {
        if (!frontR)
            frontR = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        //uma SENHORA gambiarra, mas pelo jeito nao da ruim
        frontR.size += new Vector2(Time.deltaTime * Speed, 0f);
    }
}