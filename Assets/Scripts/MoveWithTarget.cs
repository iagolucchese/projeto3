﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWithTarget : MonoBehaviour
{
    public Transform target;
    private Vector3 movementLastFrame;
    public bool DoRotate = true;

    void Start()
    {
        if (!target)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
        movementLastFrame = target.position;
    }

    void Update()
    {
        transform.position += target.position - movementLastFrame;
        movementLastFrame = target.position;
        if (DoRotate) transform.rotation = Quaternion.Euler(0f, ((target.localScale.x<0) ? 180f : 0f), 0f);
    }
}
