﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    private static AudioController _instance;
	public static AudioController Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("No AudioController instance.");
            }
            return _instance;
        }
    }

    private AudioSource SFXSource;
    [Range(0f,1f)]
    public float SFXVolume = 1f;
    private AudioSource SFXSource2;
    [Range(0f,1f)]
    public float SFX2Volume = 1f;
    private AudioSource MusicSource;
    [Range(0f,1f)]
    public float MusicVolume = 0.2f;
    public string DefaultGameMusicName = "GameMusic1";

    public List<AudioClip> AllSFX;
    public List<AudioClip> AllMusic;

    private float FadeOutLerp = 0f;
    private float FadeOutSpeed = 1f;
    private bool MusicFadingOut = false;

    private void Awake() 
    {
        //singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        if (!SFXSource)
        {
            SFXSource = (AudioSource)gameObject.AddComponent(typeof(AudioSource));
            SFXSource.volume = SFXVolume;
        }
        if (!SFXSource2)
        {
            SFXSource2 = (AudioSource)gameObject.AddComponent(typeof(AudioSource));
            SFXSource2.volume = SFX2Volume;
        }
        if (!MusicSource)
        {
            MusicSource = (AudioSource)gameObject.AddComponent(typeof(AudioSource));
            MusicSource.loop = true;
            MusicSource.volume = MusicVolume;
        }
        
        var sfx = Resources.LoadAll("SFX");
        foreach (var item in sfx)
        {
            AllSFX.Add(item as AudioClip);
        }

        var music = Resources.LoadAll("Music");
        foreach (var item in music)
        {
            AllMusic.Add(item as AudioClip);
        }

        //DEBUG
        if (!string.IsNullOrEmpty(DefaultGameMusicName)) 
            PlayMusic(DefaultGameMusicName);
        else
        {
            Debug.LogWarning("No default game music.");
        }
    }

    public void PlaySound(int soundIndex)
    {
        var clip = AllSFX[soundIndex];
        if (!clip) {Debug.LogError("SFX index not found"); return;}
        SFXSource.clip = clip;
        SFXSource.Play();
    }

    public void PlaySound(string soundName)
    {
        var clip = AllSFX.Find(a => a.name == soundName);
        SFXSource.clip = clip;
        SFXSource.Play();
    }

    public void PlaySound2(string soundName)
    {
        var clip = AllSFX.Find(a => a.name == soundName);
        SFXSource2.clip = clip;
        SFXSource2.Play();
    }

    public void PlayMusic(string musicName)
    {
        var clip = AllMusic.Find(a => a.name == musicName);
        MusicSource.clip = clip;
        MusicSource.Play();
    }
    
    public void PlayMusic(int musicIndex)
    {
        var clip = AllMusic[musicIndex];
        if (!clip) {Debug.LogError("Music index not found"); return;}
        MusicSource.clip = clip;
        MusicSource.Play();
    }

    public void MusicFadeOut()
    {
        MusicFadingOut = true;
    }

    private void Update() 
    {
        if (MusicFadingOut && MusicSource.isPlaying)
        {
            FadeOutLerp += Time.deltaTime * FadeOutSpeed;
            MusicSource.volume = Mathf.Lerp(MusicVolume, 0f, FadeOutLerp);
            if (FadeOutLerp >= 1)
            {
                MusicFadingOut = false;
                MusicSource.Stop();
            }
        }
    }
}
