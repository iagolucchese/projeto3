﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingTrigger : MonoBehaviour
{

    private CutsceneManager cm;

    void Start()
    {
        cm = CutsceneManager.Instance;
    }

    public void GoToEnding()
    {
        cm.PlayCutscene("EndingCutscene");
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.tag == "Player")
        {
            GoToEnding();
        }
    }
}
