﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class RevealSpotBehavior : MonoBehaviour
{
    public string CheckForTag = "Player";
    public float LerpSpeed = 1f;
    public bool HidesAgainOnExit = true;
    public bool IsRevealed = false;

    private float lerp = 0f;
    private SpriteRenderer sprite;
    private Color startingColor;
    private Color targetColor;
    private ParticleSystem ps;

    void Start()
    {
        ps = GetComponentInChildren<ParticleSystem>();
        sprite = GetComponent<SpriteRenderer>();
        startingColor = sprite.color;
        targetColor = new Color(startingColor.r, startingColor.g, startingColor.b, 0);
    }

    void Update()
    {
        if (IsRevealed && lerp <= 1)
        {
            if (ps) ps.Stop();
            lerp += Time.deltaTime * LerpSpeed;
            sprite.color = Color.Lerp(startingColor, targetColor, lerp);
        }
        else if (!IsRevealed && lerp >= 0)
        {
            if (ps) ps.Play();
            lerp -= Time.deltaTime * LerpSpeed;
            sprite.color = Color.Lerp(startingColor, targetColor, lerp);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == CheckForTag)
        {
            IsRevealed = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (HidesAgainOnExit && other.gameObject.tag == CheckForTag)
        {
            IsRevealed = false;
        }
    }
}
