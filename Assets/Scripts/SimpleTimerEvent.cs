﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SimpleTimerEvent : MonoBehaviour
{
    public float TimeToWaitInSeconds = 5f;
    public float TimerCountdown = 0f;
    public bool DoCountdown = false;
    public bool CountsDownOnStart = false;

    public UnityEvent OnTimerFinished;

    public void StartTimer(bool start = true)
    {
        DoCountdown = start;
    }

    public void ResetTimer(bool andCountdown = true)
    {
        TimerCountdown = TimeToWaitInSeconds;
        DoCountdown = andCountdown;
    }

    void Start()
    {
        ResetTimer(CountsDownOnStart);
    }

    void Update()
    {
        if (DoCountdown)
        {
            if (TimerCountdown > 0f)
                TimerCountdown -= Time.deltaTime;
            else
            {
                if (OnTimerFinished != null)
                {
                    OnTimerFinished.Invoke();
                }
                else
                {
                    Debug.LogWarning("Timer " + name + " has no OnTimerFinished event.");
                }
                DoCountdown = false;
            }
        }
    }
}
