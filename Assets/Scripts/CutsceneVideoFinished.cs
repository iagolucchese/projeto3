﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class CutsceneVideoFinished : MonoBehaviour
{
    private VideoPlayer video;
    public UnityEvent OnVideoFinished;
    private InputManager input;

    void Start()
    {
        video = GetComponent<VideoPlayer>();
        input = InputManager.Instance;
        if (video)
        {
            video.loopPointReached += EndReached;
        }
    }

    private void Update() {
        if (input != null && video != null && input.JumpButton || input.DogButton)
        {
            video.playbackSpeed = 10;
        }
        else
        {
            video.playbackSpeed = 1;
        }
    } 

    private void EndReached(VideoPlayer vp)
    {
        Debug.Log("Cutscene ended.");
        if (OnVideoFinished != null)
        {
            OnVideoFinished.Invoke();
        }
    }

    public void GoToScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    private void OnDestroy() 
    {
        if (video)
        {
            video.loopPointReached -= EndReached;
        }
    }
}
