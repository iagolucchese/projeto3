﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarknessMaskController : MonoBehaviour
{
    private DarknessMaskController _instance;
    public float alphaLerp = 0f;
    public float scaleLerp = 0f;
    private Vector3 baseScale;
    private Vector3 targetScale;

    public SpriteRenderer Darkness;
    public SpriteRenderer DarknessMask;
    public SpriteRenderer PlayerDarknessMask;
    public bool MaskIsActive = false;
    public float maskSizeMultiplier = 1.2f;

    private InputManager input;

    void Start()
    {
        input = InputManager.Instance;
        
        if (!Darkness)
        {
            Darkness = GameObject.Find("Darkness").GetComponent<SpriteRenderer>();
            if (!Darkness) Debug.LogWarning("Dark background not found");
        }
        if (!DarknessMask)
        {
            DarknessMask = GameObject.Find("DarknessMask").GetComponent<SpriteRenderer>();
            if (!DarknessMask) Debug.LogWarning("Darkness mask not found");
        }
        if (!PlayerDarknessMask)
        {
            PlayerDarknessMask = GameObject.Find("PlayerDarknessMask").GetComponent<SpriteRenderer>();
            if (!PlayerDarknessMask) Debug.LogWarning("Player's Darkness mask not found");
        }
        baseScale = DarknessMask.transform.localScale;
        targetScale = baseScale * maskSizeMultiplier;
    }

    void Update()
    {
        targetScale = baseScale * maskSizeMultiplier;
        if (FollowerBehavior.Instance.doFollow)//DarknessMask.gameObject.activeInHierarchy)
        {
            PlayerDarknessMask.gameObject.SetActive(false);
        }
        else
        {
            PlayerDarknessMask.gameObject.SetActive(true);
        }
        if (MaskIsActive)
        {
            alphaLerp += Time.deltaTime;
            if (alphaLerp > 1f) alphaLerp = 1f;
            Darkness.color = Color.Lerp(Color.clear, Color.black, alphaLerp);
            DarknessMask.color = Color.Lerp(Color.clear, Color.white, alphaLerp);
            PlayerDarknessMask.color = Color.Lerp(Color.clear, Color.white, alphaLerp);
        }
        else
        {
            alphaLerp -= Time.deltaTime;
            if (alphaLerp < 0f) alphaLerp = 0f;
            Darkness.color = Color.Lerp(Color.clear, Color.black, alphaLerp);
            DarknessMask.color = Color.Lerp(Color.clear, Color.white, alphaLerp);
            PlayerDarknessMask.color = Color.Lerp(Color.clear, Color.white, alphaLerp);
        }
        if (input.DogButton)
        {
            scaleLerp += Time.deltaTime;
            if (scaleLerp > 1f) scaleLerp = 0.9f;
            
            DarknessMask.transform.localScale = Vector3.LerpUnclamped(baseScale, targetScale, scaleLerp);
        }
        else
        {
            scaleLerp -= Time.deltaTime;
            if (scaleLerp < 0f) scaleLerp = 0f;
            DarknessMask.transform.localScale = Vector3.LerpUnclamped(baseScale, targetScale, scaleLerp);            
        }
    }
}
