﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleTrapManager : MonoBehaviour
{
    public List<SpikeTrapBehavior> AllTraps;
    public float DelayBetweenTrapActivations = 1f;
    public bool ActivateTrapsOnStart = true;

    private int i = 0;

    void Start()
    {
        if (ActivateTrapsOnStart)
            StartAllTraps();
    }

    public void StartAllTraps()
    {
        AllTraps[i].TurnOnTrap();
        i++;
        if (i < AllTraps.Count)
            Invoke("StartAllTraps", DelayBetweenTrapActivations);
    }

    public void StopAllTraps()
    {
        foreach (SpikeTrapBehavior trap in AllTraps)
        {
            trap.TurnOffTrap();
        }
    }
}
