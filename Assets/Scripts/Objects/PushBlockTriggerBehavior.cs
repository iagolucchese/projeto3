﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushBlockTriggerBehavior : MonoBehaviour
{
    public bool IsLeft = false;

    private PushBlockBehavior pbb;
    private PlayerController player;
    private InputManager input;
    private bool IsBeingPushed = false;

    void Start()
    {
        pbb = transform.parent.GetComponent<PushBlockBehavior>();
        player = PlayerController.Instance;
        input = InputManager.Instance;
    }

    private void OnTriggerStay2D(Collider2D other) 
    {
        if (other.gameObject.tag == "Player")
        {
            if (player._controller.isGrounded)
            {
                if (input.HorizontalInput > 0 && IsLeft)
                {
                    pbb.DirectionOfPush = 1f;
                    if (!IsBeingPushed)
                    {
                        player.StartedPushing();
                        IsBeingPushed = true;
                    }
                }
                else if (input.HorizontalInput < 0 && !IsLeft)
                {
                    pbb.DirectionOfPush = -1f;
                    if (!IsBeingPushed)
                    {
                        player.StartedPushing();
                        IsBeingPushed = true;
                    }
                }
                else
                {
                    pbb.DirectionOfPush = 0f;
                    if (IsBeingPushed)
                    {
                        player.StoppedPushing();
                        IsBeingPushed = false;
                    }
                }
            }
            else
            {
                pbb.DirectionOfPush = 0f;
                if (IsBeingPushed)
                {
                    player.StoppedPushing();
                    IsBeingPushed = false;
                }
            }
        }       
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
        {
            pbb.DirectionOfPush = 0f;
            if (IsBeingPushed)
            {
                player.StoppedPushing();
                IsBeingPushed = false;
            }
        }
    }
}
