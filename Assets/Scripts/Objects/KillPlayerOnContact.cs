﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KillPlayerOnContact : MonoBehaviour
{
    public float resetInSeconds = 0.5f;

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
        {
            PlayerController.Instance.HandlePlayerDeath(resetInSeconds);
        }
    }
}
