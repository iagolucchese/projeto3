﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorShardBehavior : MonoBehaviour
{
    public bool Activated = false;
    public float DeactivateAfterSeconds = 3f;

    private Animator anim;

    private void Start() {
        anim = GetComponent<Animator>();
    }

    public void ActivateShard()
    {
        if (!Activated)
        {
            Activated = true;
            GateMirrorBehavior.ShardsCollected++;

            GetComponent<Collider2D>().enabled = false;
            if (anim)
                anim.Play("ShardCollected");
            Invoke("DeactivateShard", DeactivateAfterSeconds);
            CutsceneManager.Instance.PlayCutscene("MirrorShardGet");
        }
    }

    private void DeactivateShard()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.tag == "Player")
        {
            ActivateShard();
        }
    }
}
