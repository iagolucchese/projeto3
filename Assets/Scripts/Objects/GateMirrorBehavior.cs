﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateMirrorBehavior : MonoBehaviour
{
    public const int ShardsToWin = 4;
    public static int ShardsCollected = 0;
    public bool IsPlayerNear = false;
    public bool CanInteractWith = true;
    public List<SpriteRenderer> AllShardSprites;

    private Animator anim;
    private ButtonPromptBehavior buttonPrompt;
    private InputManager input;
    private PlayerController player;

    void Start()
    {
        anim = GetComponent<Animator>();
        buttonPrompt = GetComponent<ButtonPromptBehavior>();
        input = InputManager.Instance;
        player = PlayerController.Instance;
    }

    void Update()
    {
        if (ShardsCollected >= ShardsToWin)
        {
            CanInteractWith = true;
            buttonPrompt.CanInteractWith = true;
            if (IsPlayerNear && input.DogButtonDown)
            {
                CanInteractWith = false;
                buttonPrompt.CanInteractWith = false;
                player.BlockPlayerMovement();
                
                AudioController.Instance.MusicFadeOut();
                CutsceneManager.Instance.PlayCutscene("FinalRoomEnter");
            }
        }
        else
        {
            CanInteractWith = false;
            buttonPrompt.CanInteractWith = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.tag == "Player")
        {
            IsPlayerNear = true;
            anim.SetBool("ShouldMove", true);
            for (int i = 0; i < AllShardSprites.Count; i++)
            {
                if (i < ShardsCollected)
                    AllShardSprites[i].gameObject.SetActive(true);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
        {
            IsPlayerNear = false;
            anim.SetBool("ShouldMove", false);
        }
    }
}
