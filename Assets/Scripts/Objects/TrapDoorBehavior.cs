﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapDoorBehavior : MonoBehaviour
{
    //public float OpeningSpeed = 1f;

    private Animator anim;
    new AudioSource audio;
    public float OpenWithDelay = 0f;
    public bool StartClosed = true;

    void Start()
    {
        anim = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        if (!StartClosed)
            anim.SetBool("Open", true);
        else
            anim.SetBool("Open", false);
    }

    public void OpenDoor()
    {
        Invoke("Open", OpenWithDelay);
    }

    private void Open()
    {
        anim.SetBool("Open", true);
        if (audio)
        {           
            if (audio.enabled)            
                audio.Play();               
            else
                audio.enabled = true;
        }
        //anim.SetFloat("Speed", OpeningSpeed);
    }

    public void CloseDoor()
    {
        anim.SetBool("Open", false);
        if (audio)
        {           
            if (audio.enabled)            
                audio.Play();            
            else
                audio.enabled = true;
        }
        //anim.SetFloat("Speed", OpeningSpeed);
    }
}