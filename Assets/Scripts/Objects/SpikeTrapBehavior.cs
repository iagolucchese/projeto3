﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeTrapBehavior : MonoBehaviour
{
    public bool IsTurnedOn = false;
    public float RaiseSpeed = 1f;
    public float RetractSpeed = 1f;
    public float DelayBetweenRaises = 0f;
    public Vector3 RaisedTargetPosition = Vector3.zero;
    public Transform Spike;

    private Animator animator;
    private Vector3 startingPosition;
    private float lerp = 0f;
    private float delay = 0f;
    public bool raising;
    public bool IsDoor = false;

    void Start()
    {
        //animator = GetComponent<Animator>();
        startingPosition = Spike.localPosition;
    }

    void Update()
    {
        /* animator.speed = RaiseSpeed;
        animator.SetBool("DoRaise", IsTurnedOn); */

        if (IsTurnedOn && !raising && lerp <= 0)
        {
            if (delay > 0)
                delay -= Time.deltaTime;
            else
            {
                raising = true;
            } 
        }
        else if (!IsDoor && raising && lerp >= 1)
        {
            delay = DelayBetweenRaises;
            raising = false;
        }
        if (raising)
            lerp += RaiseSpeed * Time.deltaTime;
        else
            lerp -= RetractSpeed * Time.deltaTime;
        lerp = Mathf.Clamp01(lerp);
        Spike.localPosition = Vector3.Lerp(startingPosition, RaisedTargetPosition, lerp);
    }

    public void TurnOnTrap()
    {
        IsTurnedOn = true;
    }

    public void TurnOffTrap()
    {
        IsTurnedOn = false;
    }

    public void IsRaising(bool raise)
    {
        raising = raise;
    }
}
