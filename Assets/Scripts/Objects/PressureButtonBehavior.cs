﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PressureButtonBehavior : MonoBehaviour
{
    public int IsBeingPressed = 0;
    public bool IsPressed = false;
    public bool StaysPressed = true;
    public float PressSpeed = 1f;
    public float UnpressSpeed = 1f;
    public Vector3 PressDistance = new Vector3(0f, -0.2f, 0f);
    public List<string> TagsToCheck;
    public float lerp = 0f;
    public Transform ObjectToPress;

    public UnityEvent OnFullyPressed;
    public UnityEvent OnUnpressed;

    private Vector3 StartingPosition;
    new AudioSource audio;

    void Start()
    {
        StartingPosition = ObjectToPress.position;
        audio = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        if (!IsPressed || !StaysPressed)
        {
            if (IsBeingPressed > 0)
            {
                if (lerp < 1f)
                {
                    lerp += Time.fixedDeltaTime * PressSpeed;
                }
                else
                {
                    if (!IsPressed)
                    {
                        IsPressed = true;
                        if (audio)
                            audio.Play();
                        if (OnFullyPressed != null)
                            OnFullyPressed.Invoke();
                    }
                }
            } 
            else
            {
                lerp -= Time.fixedDeltaTime * UnpressSpeed;
            }

            lerp = Mathf.Clamp01(lerp);
            ObjectToPress.position = Vector3.Lerp(StartingPosition, StartingPosition + PressDistance, lerp);

            if (lerp < 1f && !StaysPressed && IsPressed && IsBeingPressed <= 0)
            {
                UnpressButton();
            }
        }
    }

    public void UnpressButton()
    {
        IsPressed = false;
        if (OnUnpressed != null)
            OnUnpressed.Invoke();
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        foreach (string tag in TagsToCheck)
        {
            if (other.gameObject.tag == tag)
            {
                IsBeingPressed++;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) 
    {
        foreach (string tag in TagsToCheck)
        {
            if (other.gameObject.tag == tag)
            {
                IsBeingPressed--;
            }
        }
    }
}
