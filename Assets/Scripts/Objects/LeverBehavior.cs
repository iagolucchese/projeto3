﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LeverBehavior : MonoBehaviour
{
    public bool IsFlippedOn = false;
    public bool CanInteractWith = true;
    public bool CanBeUsedOnlyOnce = false;
    public UnityEvent LeverFlipped;
    public UnityEvent LeverUnflipped;

    private Animator animator;
    private bool IsPlayerNear;
    private InputManager input;
    new AudioSource audio;
    private PlayerController player;
    private float lerp;
    private ButtonPromptBehavior buttonPrompt;

    void Start()
    {
        animator = GetComponent<Animator>();
        input = InputManager.Instance;
        audio = GetComponent<AudioSource>();
        player = PlayerController.Instance;
        buttonPrompt = GetComponent<ButtonPromptBehavior>();
    }

    void Update()
    {
        if (IsPlayerNear && CanInteractWith && input.DogButtonDown) //usando o botao do cachorro por enquanto(?)
        {
            IsFlippedOn = !IsFlippedOn;
            animator.SetBool("IsFlippedOn", IsFlippedOn);

            CanInteractWith = false;
            buttonPrompt.CanInteractWith = false;

            player.BlockPlayerMovement();
            if (audio) audio.Play();
        }
    }

    public void ToggleLever()
    {
        if (!CanBeUsedOnlyOnce)
        {
            CanInteractWith = true;
            buttonPrompt.CanInteractWith = true;
        }
        player.UnblockPlayerMovement(true, false, false);
        if (IsFlippedOn)
            LeverFlipped.Invoke();
        else
            LeverUnflipped.Invoke();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
        {
            IsPlayerNear = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
        {
            IsPlayerNear = false;
        }
    }
}
