﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupBehavior : MonoBehaviour
{
    public enum PowerupTypes
    {
        WallJump, Dash, DashReset
    }

    public PowerupTypes PowerupType;
    public float delayToRespawn = 1f;
    private PlayerController pc;
    private SpriteRenderer sr;
    private Collider2D col;
    private CutsceneManager cm;
    private Animator anim;
    new AudioSource audio;

    private void Start()
    {
        pc =  PlayerController.Instance;
        cm = CutsceneManager.Instance;
        sr = GetComponent<SpriteRenderer>();
        col = GetComponent<Collider2D>();
        anim = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        switch (PowerupType)
        {
            case PowerupTypes.WallJump:
                anim.Play("WalljumpPWSpin");
                break;
            case PowerupTypes.Dash:
                anim.Play("DashPWSpin");
                break;
            case PowerupTypes.DashReset:
                anim.Play("DashResetSpin");
                break;
        }
    }

    private void ActivatePowerup()
    {
        switch (PowerupType)
        {
            case PowerupTypes.WallJump:
                pc.UnlockWalljump();
                cm.PlayCutscene("WallJumpGet");
                Destroy(this.gameObject);
                break;
            case PowerupTypes.Dash:
                pc.UnlockDash();
                cm.PlayCutscene("DashGet");
                Destroy(this.gameObject);
                break;
            case PowerupTypes.DashReset:
                pc.ResetDash();
                sr.enabled = false;
                Invoke("ReEnablePowerup", delayToRespawn);
                break;
            default:
                Debug.LogError("Powerup has no type.");
                break;
        }
        GetComponent<Collider2D>().enabled = false;
        if (audio)
            audio.Play();
    }

    private void ReEnablePowerup()
    {
        //TODO animacao de powerup voltando
        sr.enabled = true;
        col.enabled = true;
    }

    public void ShowPowerup()
    {
        gameObject.SetActive(true);
        //TODO animacao cheia de balaca
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
            ActivatePowerup();
    }
}
