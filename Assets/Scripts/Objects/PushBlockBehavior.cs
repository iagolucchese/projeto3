﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class PushBlockBehavior : MonoBehaviour
{
    public float PushingSpeed = 1f;
    public float PushingAccel = 0.1f;
    [Range(-1f, 1f)]
    public float DirectionOfPush = 0f;
    public float speed = 0f;
    public float gravity = 100f;
    public bool grounded;

    private CharacterController2D _controller;
    private Vector2 _velocity;
    private ParticleSystem dustParticles;

    void Start()
    {
        _controller = GetComponent<CharacterController2D>();
        dustParticles = GetComponentInChildren<ParticleSystem>();
    }

    void FixedUpdate()
    {
        grounded = _controller.isGrounded;
        if (_controller.isGrounded)        
            _velocity.y = 0;   

        if (DirectionOfPush != 0)
        {            
            speed += PushingAccel;
            if (!dustParticles.isPlaying)
            {
                //Debug.Log("started pushing");
                dustParticles.Play();
            }
        }
        else
        {
            speed = 0f;
            if (dustParticles.isPlaying)
            {
                dustParticles.Stop();
                //Debug.Log("stopped pushing");
            }
        }
        speed = Mathf.Clamp(speed, 0f, PushingSpeed);
        _velocity.x = DirectionOfPush * speed * Time.fixedDeltaTime;        
        _velocity.y += gravity * Time.fixedDeltaTime;

        _controller.move(_velocity * Time.fixedDeltaTime);
        _velocity = _controller.velocity;
    }
}
