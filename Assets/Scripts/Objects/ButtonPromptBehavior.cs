﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class ButtonPromptBehavior : MonoBehaviour
{
    public SpriteRenderer KBKey;
    public SpriteRenderer CONKey;
    public bool IsPlayerNear = false;
    public bool CanInteractWith = true;
    public float AlphaSpeed = 3f;
    
    private float lerp;
    private InputManager input;

    void Start()
    {
        input = InputManager.Instance;
    }

    void Update()
    {
        if (IsPlayerNear && CanInteractWith)
        {
            lerp += Time.deltaTime * AlphaSpeed;
            
            if (input.IsUsingKeyboard)
            {
                KBKey.gameObject.SetActive(true);
                CONKey.gameObject.SetActive(false);
            }
            else
            {
                KBKey.gameObject.SetActive(false);
                CONKey.gameObject.SetActive(true);
            }
        }
        else
        {
            lerp -= Time.deltaTime * AlphaSpeed;
        }
        lerp = Mathf.Clamp01(lerp);
        KBKey.color = Color.Lerp(Color.clear, Color.white, lerp);
        CONKey.color = Color.Lerp(Color.clear, Color.white, lerp);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
        {
            IsPlayerNear = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
        {
            IsPlayerNear = false;
        }
    }
}
