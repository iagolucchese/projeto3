﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Tilemaps;

public class DarkTilesBehavior : MonoBehaviour
{
    public float lerp = 0f;
    public bool IsEnabled = true;
    //private Tilemap tiles;
    private SpriteRenderer sprite;
    new Collider2D collider;

    void Start()
    {
        //tiles = GetComponent<Tilemap>();
        sprite = GetComponent<SpriteRenderer>();
        collider = GetComponent<Collider2D>();
        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 255);
    }

    void Update()
    {
        if (!IsEnabled && lerp <= 1)
        {
            lerp += Time.deltaTime;
            sprite.color = Color.Lerp(sprite.color, new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0), lerp);
        }
    }

    public void DisableDarkTiles()
    {
        collider.enabled = false;
        IsEnabled = false;
        //TODO audio ?
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.tag == "AntiDarkTile")
        {
            if (FollowerBehavior.Instance.doFollow)
                DisableDarkTiles();
        }
    }
}
