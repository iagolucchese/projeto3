﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;

public class CutsceneManager : MonoBehaviour
{
    private static CutsceneManager _instance;
    public static CutsceneManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = (CutsceneManager)GameObject.FindObjectOfType(typeof(CutsceneManager));
                if (_instance == null)
                {
                    GameObject ic = new GameObject("Cutscene Manager");
                    _instance = ic.AddComponent<CutsceneManager>();
                }
            }

            return _instance;
        }
    }

    public SpriteRenderer TransitionPanel;
    public CinemachineVirtualCamera CGVcam;
    public Animator CutsceneAnimator;
    public Image DialogueImage;
    public Image TutorialImage;

    public List<Sprite> AllDialogueSprites;
    public List<Sprite> AllPowerupTutorials;

    private PlayerController Player;
    private bool DoPanelTransition = false;
    private float lerp = 0f;
    private float transitionSpeed = 1f;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        Player = PlayerController.Instance;


        if (!TransitionPanel)
            Debug.LogWarning("Cutscene Manager has no TransitionPanel");
        if (!CGVcam)
            Debug.LogWarning("Cutscene Manager has no CGVcam");
        if (!CutsceneAnimator)
            Debug.LogWarning("Cutscene Manager has no CutsceneAnimator");
        if (!DialogueImage)
            Debug.LogWarning("Cutscene Manager has no DialogueImage");
        if (!TutorialImage)
            Debug.LogWarning("Cutscene Manager has no TutorialImage");
    }

    void Update() 
    {
        if (DoPanelTransition)
        {
            if (lerp < 1)
            {
                lerp += Time.deltaTime * transitionSpeed;
                TransitionPanel.color = Color.Lerp(Color.clear, Color.black, lerp);
            }
        }
    }

    public void PlayCutscene(string cutsceneName)
    {
        Debug.Log("Play cutscene: " + cutsceneName);
        Invoke(cutsceneName, 0f);
    }

    //cutscene functions
    private void FinalRoomEnter()
    {
        DeathTransitionPanelBehavior.Instance.DoDeathScreenTransition(2);
    }

    private void MirrorShardGet()
    {
        Player.BlockPlayerMovement(false, true, true);
        
        switch(GateMirrorBehavior.ShardsCollected)
        {
            case 1:
                DialogueImage.sprite = AllDialogueSprites[0];
                break;
            case 2:
                DialogueImage.sprite = AllDialogueSprites[1];
                break;
            case 3:
                DialogueImage.sprite = AllDialogueSprites[2];
                break;
            case 4:
                DialogueImage.sprite = AllDialogueSprites[3];
                break;
            default:
                DialogueImage.sprite = AllDialogueSprites[0]; //debug?
                break;
        }
        CutsceneAnimator.Play("CGMirrorShardZoomIn");
    }

    public void CutsceneEnded()
    {
        Player.UnblockPlayerMovement(false, false, true);
    }

    private void WallJumpGet()
    {
        Player.BlockPlayerMovement(false, true, true);
        CutsceneAnimator.Play("CGPowerupZoomIn");
        TutorialImage.sprite = AllPowerupTutorials[0];
    }

    private void DashGet()
    {
        Player.BlockPlayerMovement(false, true, true);
        CutsceneAnimator.Play("CGPowerupZoomIn");
        TutorialImage.sprite = AllPowerupTutorials[1];
    }

    private void CuscoGet()
    {

    }

    private void EndingCutscene()
    {
        Player.BlockPlayerMovement();
        DoPanelTransition = true;
        Invoke("GoToFinalScene", 4f);
    }

    private void GoToFinalScene()
    {
        SceneController.Instance.GoToScene("EndingCutscene");
    }
}
