﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private static InputManager _instance;
    public static InputManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = (InputManager)GameObject.FindObjectOfType(typeof(InputManager));
                if (_instance == null)
                {
                    GameObject ic = new GameObject("Input Manager");
                    _instance = ic.AddComponent<InputManager>();
                }
            }

            return _instance;
        }
    }

    public float HorizontalInput = 0f;
    public float VerticalInput = 0f;
    public bool JumpButtonDown = false;
    public bool JumpButton = false;
    public bool DashButtonDown = false;
    public bool DogButtonDown = false;
    public bool DogButton = false;
    public bool IsUsingKeyboard = false;

    private float f;

    void Update()
    {
        //---movement axis---
        //horizontal
        if (Input.GetAxis("HorizontalCon") != 0)
        {
            HorizontalInput = Mathf.Round(Input.GetAxis("HorizontalCon") + 1) - 1;
            IsUsingKeyboard = false;
        }
        else if (Input.GetAxis("HorizontalDPad") != 0)
        {
            HorizontalInput = Mathf.Round(Input.GetAxis("HorizontalDPad") + 1) - 1;
            IsUsingKeyboard = false;
        }
        else if (Input.GetAxis("HorizontalKB") != 0)
        {
            HorizontalInput = Mathf.Round(Input.GetAxis("HorizontalKB") + 1) - 1;
            IsUsingKeyboard = true;
        }
        else
        {
            HorizontalInput = 0;
        }

        //vertical
        if (Input.GetAxis("VerticalCon") != 0)
        {
            VerticalInput = Mathf.Round(Input.GetAxis("VerticalCon") + 1) - 1;
            IsUsingKeyboard = false;
        }
        else if (Input.GetAxis("VerticalDPad") != 0)
        {
            VerticalInput = Mathf.Round(Input.GetAxis("VerticalDPad") + 1) - 1;
            IsUsingKeyboard = false;
        }
        else if (Input.GetAxis("VerticalKB") != 0)
        {
            VerticalInput = Mathf.Round(Input.GetAxis("VerticalKB") + 1) - 1;
            IsUsingKeyboard = true;
        }
        else
        {
            VerticalInput = 0;
        }

        //---action buttons---
        //jump down
        if (Input.GetButtonDown("JumpCon"))
        {
            JumpButtonDown = Input.GetButtonDown("JumpCon");
            IsUsingKeyboard = false;
        }
        else if (Input.GetButtonDown("JumpKB"))
        {
            JumpButtonDown = Input.GetButtonDown("JumpKB");
            IsUsingKeyboard = true;
        }
        else
        {
            JumpButtonDown = false;
        }

        //jump
        if (Input.GetButton("JumpCon"))
        {
            JumpButton = Input.GetButton("JumpCon");
            IsUsingKeyboard = false;
        }
        else if (Input.GetButton("JumpKB"))
        {
            JumpButton = Input.GetButton("JumpKB");
            IsUsingKeyboard = true;
        }
        else
        {
            JumpButton = false;
        }

        //dash down
        if (Input.GetAxis("DashTrigger") >= 1f)
        {
            DashButtonDown = Input.GetAxis("DashTrigger") >= 1f;
            IsUsingKeyboard = false;
        }
        else if (Input.GetButtonDown("DashCon"))
        {
            DashButtonDown = Input.GetButtonDown("DashCon");
            IsUsingKeyboard = false;
        }
        else if (Input.GetButtonDown("DashKB"))
        {
            DashButtonDown = Input.GetButtonDown("DashKB");
            IsUsingKeyboard = true;
        }
        else
        {
            DashButtonDown = false;
        }

        //dog down
        if (Input.GetButtonDown("DogCon"))
        {
            DogButtonDown = Input.GetButtonDown("DogCon");
            IsUsingKeyboard = false;
        }
        else if (Input.GetButtonDown("DogKB"))
        {
            DogButtonDown = Input.GetButtonDown("DogKB");
            IsUsingKeyboard = true;
        }
        else
        {
            DogButtonDown = false;
        }

        //dog
        if (Input.GetButton("DogCon"))
        {
            DogButton = Input.GetButton("DogCon");
            IsUsingKeyboard = false;
        }
        else if (Input.GetButton("DogKB"))
        {
            DogButton = Input.GetButton("DogKB");
            IsUsingKeyboard = true;
        }
        else
        {
            DogButton = false;
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}
