﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

[System.Serializable]
[RequireComponent(typeof(BoxCollider))]
public class RoomManager : MonoBehaviour
{
    public CinemachineVirtualCamera RoomCamera;
    public GameObject vcamPrefab;
    public List<CheckpointBehavior> RoomCheckpoints;
    public int RoomNumber = -1;
    public bool IsDarkRoom = false;
    public float SetPlayerGroundDamping = 0f;
    public List<GameObject> backgroundParents;
    private PlayerController player;

    void Awake()
    {       
        player = PlayerController.Instance;
        RoomCamera = GetComponentInChildren<CinemachineVirtualCamera>();
        /* if (RoomNumber < 0)
            RoomNumber = SceneController.NumberOfRooms++; */
        if (!SceneController.Instance.AllRoomsOnScene.Contains(this))
            SceneController.Instance.AllRoomsOnScene.Add(this);
        RoomNumber = SceneController.Instance.AllRoomsOnScene.IndexOf(this);

        RoomCheckpoints = new List<CheckpointBehavior>();
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            if (child.tag == "Checkpoint")
            {
                RoomCheckpoints.Add(child.gameObject.GetComponent<CheckpointBehavior>());
            }
        }

        if (!RoomCamera)
        {
            if (!vcamPrefab)
            {
                vcamPrefab = (GameObject)Resources.Load("Prefabs/vcamPrefab");
                if (!vcamPrefab)
                    Debug.LogError("Cade o prefab da VCAM");
            }
            //cria uma vcam para a sala
            RoomCamera = Instantiate(vcamPrefab).GetComponent<CinemachineVirtualCamera>();
            RoomCamera.transform.localPosition = new Vector3(0f, 0f, -11f);
            RoomCamera.name = this.gameObject.name + "'s VCam";
            RoomCamera.transform.parent = this.transform;
            RoomCamera.GetComponent<CinemachineConfiner>().m_BoundingVolume = this.GetComponent<Collider>();
            RoomCamera.Follow = PlayerController.Instance.transform;
        }
        if (player.CurrentRoom == this)
        {
            RoomCamera.gameObject.SetActive(true);
            SceneController.Instance.ActiveCamera = RoomCamera;
            //primeiro checkpoint
            if (RoomCheckpoints.Count > 0)
                player.LatestCheckpoint = RoomCheckpoints[0];
            else
            {
                Debug.LogError("Room player started in has no checkpoint(?)");
            }
            ToggleBackgrounds(false);
        }
        else
        {
            RoomCamera.gameObject.SetActive(false);
            ToggleBackgrounds(true);
        }
        SceneController.Instance.AllVcamsOnScene.Add(RoomCamera);
    }

    public void ToggleBackgrounds(bool hide)
    {
        if (backgroundParents != null && backgroundParents.Count > 0)
        {
            backgroundParents.ForEach(x => x.SetActive(!hide));
        }
    }

    private void OnDestroy() 
    {
        var a = SceneController.Instance;
        a.AllRoomsOnScene.Remove(this);
        a.AllVcamsOnScene.Remove(RoomCamera);
    }
}
