﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Cinemachine;

[System.Serializable]
public class SceneController : MonoBehaviour
{
    private static SceneController _instance;
	public static SceneController Instance
    {
        get
        {
            if (_instance == null)
            {
                //Debug.LogError("No SceneController instance.");
            }
            return _instance;
        }
    }

    public static float PlayTime = 650f;
    public static int DeathCounter = -1;

    public List<CinemachineVirtualCamera> AllVcamsOnScene;
    public List<RoomManager> AllRoomsOnScene;
    public CinemachineVirtualCamera ActiveCamera;
    public CinemachineConfiner CGVcamConfine;

    private DarknessMaskController DMC; 
    private PlayerController player;
    private bool InTransition = false;

    void Awake()
    {
        PlayTime = 0f;
        DeathCounter = 0;

        player = PlayerController.Instance;
        DMC = GetComponent<DarknessMaskController>();

        if (AllRoomsOnScene == null)
            AllRoomsOnScene = new List<RoomManager>();
        if (!CGVcamConfine)
            CGVcamConfine = GameObject.Find("CGVcam").GetComponent<CinemachineConfiner>();

        AllVcamsOnScene = new List<CinemachineVirtualCamera>();

        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void TransitionToVCam(CinemachineVirtualCamera NextRoomCamera, bool IsDarkRoom = false)
    {
        foreach (CinemachineVirtualCamera item in AllVcamsOnScene)
        {
            item.gameObject.SetActive(false);
        }
        NextRoomCamera.gameObject.SetActive(true);
        if (IsDarkRoom)
        {
            DMC.MaskIsActive = true;
        }
        else
        {
            DMC.MaskIsActive = false;
        }
        CGVcamConfine.m_BoundingVolume = NextRoomCamera.gameObject.GetComponent<CinemachineConfiner>().m_BoundingVolume;
    }

    public void GoToLastRoom()
    {
        var finalRoom = AllRoomsOnScene[AllRoomsOnScene.Count-1];
        player.LatestCheckpoint = finalRoom.RoomCheckpoints[0];
        TransitionToVCam(finalRoom.RoomCamera);
        player.RespawnAtCheckpoint();
    }

    public static void ResetThisScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public static void DoDeathScreenTransition(float delay = 0f)
    {
        Instance.Invoke("DoTransition", delay);
    }

    private void DoTransition()
    {
        DeathTransitionPanelBehavior.Instance.DoDeathScreenTransition();
    }

    private void LateUpdate()
    {
        PlayTime += Time.deltaTime;
        //DEBUG
        if (Input.GetKeyDown("r") && !player.isDead)
        {
            player.HandlePlayerDeath();
        }
        if (ActiveCamera == null)
        {
            Debug.Log("Active camera is null, trying to find the correct one");
            TransitionToVCam(player.CurrentRoom.RoomCamera);
            ActiveCamera = player.CurrentRoom.RoomCamera;
        }
    }

    public void GoToScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName, LoadSceneMode.Single);        
    }
}
