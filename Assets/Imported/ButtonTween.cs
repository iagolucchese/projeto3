﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

// Used for tweening a button in the phone. (Movement & rotation are done locally).
public class ButtonTween : MonoBehaviour {

    [Header("Original position, scale and rotation.")]
    [SerializeField] private Vector3 originalPosition = Vector3.zero;
    [SerializeField] private Vector3 originalRotation = Vector3.zero;
    [SerializeField] private Vector3 originalScale = Vector3.zero;

    [Header("Target position, scale and rotation.")]
    [SerializeField] private Vector3 targetPosition = Vector3.zero;
    [SerializeField] private Vector3 targetRotation = Vector3.zero;
    [SerializeField] private Vector3 targetScale = Vector3.zero;

    [Header("Punch amount in position, scale and rotation.")]
    [SerializeField] private Vector3 punchAmountPosition = Vector3.zero;
    [SerializeField] private Vector3 punchAmountRotation = Vector3.zero;
    [SerializeField] private Vector3 punchAmountScale = Vector3.zero;

    [Header("Animation parameters")]
    [SerializeField] private float animationSpeed = 1f;
    [SerializeField] private int vibrato = 1;
    [SerializeField] private float elasticity = 1f;
    [SerializeField] private bool snapToGridTween = false;
    [SerializeField] private bool snapToGridPunch = false;
    [SerializeField] private RotateMode rotationMode = RotateMode.Fast;

    [Header("Types of tweening to do.")]
    [SerializeField] private bool position = false;
    [SerializeField] private bool rotation = false;
    [SerializeField] private bool scale = false;
    [SerializeField] private bool punchPosition = false;
    [SerializeField] private bool punchRotation = false;
    [SerializeField] private bool punchScale = false;

    [Header("Extra stuff")]
    [SerializeField] private bool animateOnStart = false;


    // I mean, its the start function.
    private void Start() {
        if(animateOnStart)
            OpenTween();
    }

    /// <summary>
    /// From original position to target position.
    /// </summary>    
    public void OpenTween() {        
        if(position) {            
            transform.DOLocalMove(targetPosition, animationSpeed, snapToGridTween);
        }

        if(rotation) {
            transform.DOLocalRotate(targetRotation, animationSpeed, rotationMode);
        }

        if(scale) {
            transform.DOScale(targetScale, animationSpeed);
        }

        if(punchPosition) {
            transform.DOPunchPosition(punchAmountPosition, animationSpeed, vibrato, elasticity, snapToGridPunch);
        }

        if(punchRotation) {
            transform.DOPunchRotation(punchAmountRotation, animationSpeed, vibrato, elasticity);
        }

        if(punchScale) {
            transform.DOPunchScale(punchAmountScale, animationSpeed, vibrato, elasticity);
        }
    }

    /// <summary>
    /// From target position to original position.
    /// </summary>
    public void CloseTween() {
        if(position) {
            transform.DOLocalMove(originalPosition, animationSpeed, snapToGridTween);
        }

        if(rotation) {
            transform.DOLocalRotate(originalRotation, animationSpeed, rotationMode);
        }

        if(scale) {
            transform.DOScale(originalScale, animationSpeed);
        }

        if(punchPosition) {
            transform.DOPunchPosition(punchAmountPosition, animationSpeed, vibrato, elasticity, snapToGridPunch);
        }

        if(punchRotation) {
            transform.DOPunchRotation(punchAmountRotation, animationSpeed, vibrato, elasticity);
        }

        if(punchScale) {
            transform.DOPunchScale(punchAmountScale, animationSpeed, vibrato, elasticity);
        }
    }
}
